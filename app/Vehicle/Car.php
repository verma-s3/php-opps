<?php

namespace App\Vehicle;

class Car extends RollingVehicle
{
	use Traits\RoadStart;
	use Traits\RoadStop;
}