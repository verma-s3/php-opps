<?php

namespace App\Vehicle;

class Vehicle
{
	/**
	 * type of vehicle 
	 * @var String
	 */
	protected $type;

	public function __construct($type)
	{
		$this->type = $type;
	}

	/**
	 * gettype function to get the typep attribute
	 * @return string
	 */
	public function getType(){
		return $this->type;

	}
}