<?php
namespace App\Vehicle\Exception;

class FLoatingVehicleException extends \Exception
{
	public function test()
	{
		throw new FLoatingVehicleException('This is a Floating problem');
	}
}