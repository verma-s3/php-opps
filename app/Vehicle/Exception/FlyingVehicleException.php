<?php
namespace App\Vehicle\Exception;

class FlyingVehicleException extends \Exception
{
	public function test()
	{
		throw new FlyingVehicleException('This is a Flying problem');
	}
}