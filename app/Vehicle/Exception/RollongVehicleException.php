<?php
namespace App\Vehicle\Exception;

use App\Vehicle\Exception\RollongVehicleException;


class RollongVehicleException extends \Exception
{
	public function test()
	{
		throw new RollongVehicleException('This is a Rolloing problem');
	}
}