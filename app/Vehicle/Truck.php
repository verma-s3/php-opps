<?php

namespace App\Vehicle;

use App\Vehicle\Exception\RollongVehicleException;
use App\Vehicle\Exception\FLoatingVehicleException;

class Truck extends RollingVehicle
{
	use Traits\RoadStart;
	use Traits\RoadStop;
	public function test()
	{
		throw new RollongVehicleException('This is a Rolling problem');
	}
}