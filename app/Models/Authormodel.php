<?php

namespace App\Models;

class Authormodel extends Model
{
	// private $name;
	// private $country;
	protected $table = 'author';
	protected $key = 'author_id';

	public function save($name,$country)
	{
		$query = 'INSERT into author 
				  (name, country)
				  Values
				  (:name,:country)';
	    $params = array(':name'=> $name,
						':country' => $country);
	    $stmt = static::$dbh->prepare($query);
	    $stmt->execute($params);

	    return static::$dbh->lastInsertId();
	}
}

