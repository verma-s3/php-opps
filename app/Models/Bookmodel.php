<?php

class Bookmodel extends Model
{
	/**
	 * table variable proetcted
	 * @var string
	 */
	protected $table = 'book';

	protected $key = 'book_id';

	/**
	 * Overriding parent one method
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function one($id)
	{
		$query = "SELECT BOOK.*,
					author.name as author,
					format.name as format,
					genre.name as genre,
					publisher.name as publisher
					FROM
					book
					JOIN author USING(author_id)
					JOIN format USING(format_id)
					JOIN publisher USING(publisher_id)
					JOIN genre USING(genre_id)
					WHERE {$this->key} = :id
					ORDER BY
					book.title ASC";
	    $params = array(':id' => $id);
		$stmt = static::$dbh->prepare($query);
		$stmt->execute($params);
		return $stmt->fetch(PDO::FETCH_ASSOC);
	}
}