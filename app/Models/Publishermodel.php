<?php

namespace App\Models;

class Publishermodel extends Model
{
	protected $table = 'publisher';
	protected $key = 'publisher_id';
}