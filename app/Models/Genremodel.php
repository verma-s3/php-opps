<?php

namespace App\Models;

class Genremodel extends Model
{
	protected $table = 'genre';
	protected $key = 'name';
}