<?php

// Demonstrate
class Model
{
	protected static $dbh;

	/**
	 * Intialize databse handle for all models
	 */
	public static function init(\PDO $dbh)
	{
		static::$dbh = $dbh;
	}

	/**
	 * Resturn al ll the sreults from model table
	 * @return [type] [description]
	 */
	public function all()
	{
		$query = "SELECT * from {$this->table}";
		$stmt = static::$dbh->prepare($query);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);		
	}

	public function one($id)
	{
		$query = "SELECT * from {$this->table} where {$this->key} = :id";
		$params = array(':id' => $id);
		$stmt = static::$dbh->prepare($query);
		$stmt->execute($params);
		return $stmt->fetch(PDO::FETCH_ASSOC);
	}
}