<?php 

//child class

class CarModel extends Model
{
	public function all()
	{
		$query = 'SELECT * from cars';
		$stmt = static::$dbh->prepare($query);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);

	}

	public function getDbh()
	{
		return static::$dbh;
	}

	public static function dbh()
	{
		return static::$dbh;
	}

	
}