<?php 

// magic methods

class Test
{
	public function __construct()
	{
		var_dump('__CONSTRUCT INVOKED');
	}

	public function __toString()
	{
		return 'HEY, You are treating me like a string';
	}
	public function __invoke()
	{
		var_dump('I am in voked');
	}

	public function __sleep()
	{

	}

	public function __wakeup()
	{
		
	}

	public function __get()
	{
		
	}

	public function __set()
	{
		
	}

	public function __destruct()
	{
		var_dump('__DESTRUCT INVOKED');
	}
}