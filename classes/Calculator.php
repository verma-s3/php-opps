<?php

class Calculator
{	
	// declaring number variables
	//public $num1;
	//public $num2;

	/**
	 * addition function
	 * @param [type] $num1 [description]
	 * @param [type] $num2 [description]
	 */
	public function addNum($num1,$num2)
	{
		$add = $num1+$num2;
		return $add;
	}

	/**
	 * Subtraction function
	 * @param [type] $num1 [description]
	 * @param [type] $num2 [description]
	 */
	public function subNum($num1,$num2)
	{
		$sub = $num1-$num2;
		return $sub;
	}

	/**
	 * Multiplication function
	 * @param [type] $num1 [description]
	 * @param [type] $num2 [description]
	 */
	public function mulNum($num1,$num2)
	{
		$mul = $num1*$num2;
		return $mul;
	}

	/**
	 * Division function
	 * @param [type] $num1 [description]
	 * @param [type] $num2 [description]
	 */
	public function divNum($num1,$num2)
	{
		$div = $num1/$num2;
		return $div;
	}
}