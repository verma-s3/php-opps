<?php

class Calculatorplus
{
	
	/**
	 * private variable for total
	 * @var integer
	 */
	private $total;

	/**
	 * add function 
	 * @param number $num 
	 * @return  added number
	 */
	public function add($num)
	{
		$this->total = $this->total + $num;
		return $this;
	}

	/**
	 * sub  function
	 * @param  number $num 
	 * @return subtratcted number
	 */
	public function sub($num)
	{
		$this->total = $this->total - $num;
		return $this;
	}

	/**
	 * Divison  function
	 * @param  number $num 
	 * @return divide number
	 */
	public function div($num)
	{
		$this->total = $this->total / $num;
		return $this;
	}

	/**
	 * multiplication  function
	 * @param  number $num 
	 * @return times number
	 */
	public function times($num)
	{
		$this->total = $this->total * $num;
		return $this;
	}

	/**
	 * get the total
	 * @return total
	 */
	public function get()
	{
		return $this->total;
	}
}