<?php

// Visibility
// -- private and public
// accessors
// -- setters and getters
class Page2
{	
	//bydefault it is not public or private so define it outside from class;
	// if you declare var then it is by default private
	
	var $title = "Our Mission";

	public $content =  "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

	// private properties are only accesssible fro nwithin the class.
	private $author = 'dave jones';

	/**
	 * Public getter function for author property
	 * @return author
	 */
	public function getAuthor(){
		// thr $this keyword refers to the instantiated object.
		// to access anything inside the 
		return $this->author;
	}

	public function setAuthor($author){
		$this->author = ucwords(strtolower($author));
	}
}