<?php 

//use of constructor to initiate class variables.
class Page3
{
	public $title;
	public $content;
	public $author;
	public $updated_at;
	public $created_at;

	/**
	 * constructor is used to intiate onjects, set attrivutes, etc.	
	 * function will be automatically run when it intialized.
	 * @param [type] $title      [description]
	 * @param [type] $content    [description]
	 * @param [type] $author     [description]
	 * @param [type] $update_at  [description]
	 * @param [type] $created_at [description]
	 */
	public function __construct($title, $content, $author,$created_at,$updated_at)
	{
			$this->title =$this->normalise($title);
			$this->content = $content;
			$this->author = $this->normalise($author);
			$this->created_at = $created_at;
			$this->updated_at = $updated_at;
	}

	public function normalise($string)
	{
			return ucwords(strtolower($string));
	}
}