<?php


class Book
{	
	// static properties
	static private $type = "static";
	static public $genre  = "Science Fiction";

	//these are normal properties of class
	public $title = 'Dune';
	public $author = 'Frank Herber';

	public function getAuthor()
	{
		return $this->author;
	}

	public function getTitle(){
		return $this->title;
	}


	public function getType()
	{
		// will not working for static keywords
		//return $this->type;
		// insted use static function
		return static::$type;
	}
}