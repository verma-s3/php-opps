<?php

//All classes begin with the 'class ' keyword
//Class names begin with uppercase letter and use camel case after that
//All classes should saved in a filename that matches the class name
//This is to facilitatate class Autoloading. Autoloaders look for a file
//that matches the class name.
//There can be only be one... Class per file. (PSR-2 standard)
//eg: Page.php

class Page
{
	
	

}//class ends