<?php

// load th ecomposer autoload
require __DIR__.'/vendor/autoload.php';

define('ENV','PRODUCTION');// dvelopemnt, production, testing

date_default_timezone_get('America/Winnipeg');

ob_start();

session_start();

// set error display and reporting level
// could be set manually inside php.ini
// which would make them global

ini_set("display_errors",1);
ini_set('error_reporting',E_ALL);

// set Database Credentials

// define('DB_DSN', 'mysql:host=localhost;dbname=booksite');
// define('DB_USER', 'root');
// define('DB_PASS', '');


// $dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
// $dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
require 'functions.php';
?>