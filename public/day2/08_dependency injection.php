<?php


/* Inversion of control -- don't let classes handle own their own dependencies. FOr example, if a class needs a DB Connection, DOn't let the class create itself...... instead, inject a DB connection into th eclass. */
class test
{
	public function __construct($array, $dbh)
	{
		$this->post = $array;
		$this->dbh = $dbh;
	}

	public function unique($value, $dbh, $table, $field)
	{

	}
}