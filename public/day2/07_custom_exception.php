<?php 

// we can easly create our own custom exception for throwing
// very specific exception 

require __DIR__ . '/../../config.php';

use App\Vehicle\Truck;
use App\Vehicle\Boat;
use App\Vehicle\Plane;
use App\Vehicle\Car;

try{
	// $t = new Truck('F150');
	// $t->test();

	$b = new Boat('creuize');
	$b->test();
}catch(App\Vehicle\Exception\FLoatingVehicleException $e){
	echo "<h2>it is a floating ecxeption</h2>";
	echo $e->getMessage();
}catch(App\Vehicle\Exception\FlyingVehicleException $e){
	echo "<h2>it is a flying ecxeption</h2>";
	echo $e->getMessage();
}catch(App\Vehicle\Exception\RollongVehicleException $e){
	echo "<h2>it is a rolling ecxeption</h2>";
	echo $e->getMessage();
	echo "<pre>";
	print_r($e->getTrace());
	echo "</pre>";
}catch(Exception $e){
	echo "<h2>it is a regular ecxeption </h2>";
	echo $e->getMessage();
}finally{
	echo "<h3>Finally.........</h3>";
}