<?php

//config file conyains our autoloader
require __DIR__."/../../config.php";

use App\Vehicle\Boat;
use App\Vehicle\Plane;
use App\Vehicle\Car;
use App\Vehicle\Truck;


$boat = new Boat('rowboat');
var_dump($boat);

$car = new Car('corvette');
var_dump($car);

$plane = new Plane('jet');
var_dump($plane);

$truck = new Truck('Ford F150');
var_dump($truck);



?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title></title>
</head>
<body>
	<p><?=$boat->getType()?></p>
	<p><?=$car->getType()?></p>
	<p><?=$plane->getType()?></p>
	<p><?=$truck->getType()?></p>
	<h2>Starting nad Moving</h2>
	<h3>Cars And Trucks</h3>
	<?php var_dump($car->start()); ?>
	<?php var_dump($car->stop()); ?>
	<?php var_dump($truck->start()); ?>
	<?php var_dump($truck->stop()); ?>
	<p></p>
</body>
</html>
