<?php 

// including the reuired files
require __DIR__ . '/../../config.php';

require __DIR__ . '/../../classes/Model.php';
require __DIR__ . '/../../classes/Bookmodel.php';
require __DIR__ . '/../../classes/Authormodel.php';
require __DIR__ . '/../../classes/Publishermodel.php';
require __DIR__ . '/../../classes/Formatmodel.php';
require __DIR__ . '/../../classes/Genremodel.php';


//Ddependencided injection  -  best pracrice is to inject into classes and methods any dependeinces thry require.
//This is sa tactic of th einversion of control design pattern.

Model::init($dbh);

$bookmodel = new Bookmodel();
$authormodel = new Authormodel();
$publishermodel = new Publishermodel();
$formatmodel = new Formatmodel();
$genremodel = new Genremodel();

//dd($bookmodel->all());
//dd($authormodel->all());

dd($bookmodel->one(3));
dd($authormodel->one(3));
dd($publishermodel->one(3));
dd($genremodel->one('SF'));
dd($formatmodel->one('Paper'));

