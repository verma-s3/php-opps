<?php

require __DIR__ . '/../../config.php';

require __DIR__ . '/../../classes/Page2.php';

// instantiated the object
$page = new Page2();
dd($page->getAuthor());
//$page->setAuthor('steve george');
?><!DOCTYPE html>
<html lang='en'>
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
	<title></title>
</head>
<body>
	<h1><?=esc($page->title)?></h1>
	<small><?=esc($page->getAuthor())?></small>
	<p><?=esc($page->content)?></p>
</body>
</html>